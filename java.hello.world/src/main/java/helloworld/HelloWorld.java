package helloworld;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class HelloWorld {
  public static void main(String args[]){
    try (InputStream input = HelloWorld.class.getClassLoader().getResourceAsStream("helloworld.properties")) {
      Properties prop = new Properties();
      prop.load(input);
      System.out.println(prop.getProperty("hello.world.message"));
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
